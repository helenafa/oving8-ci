import { Given, When, Then, And } from "cypress-cucumber-preprocessor/steps";


//scenario1  
Given(/^at jeg har åpnet nettkiosken$/, () => {
    cy.visit('http://localhost:8080');
});

When(/^jeg legger inn varer og kvanta$/, () => {
    cy.get('#product').select('Hubba bubba');
    cy.get('#quantity').clear().type('4');
    cy.get('#saveItem').click();

    cy.get('#product').select('Smørbukk');
    cy.get('#quantity').clear().type('5');
    cy.get('#saveItem').click();

    cy.get('#product').select('Stratos');
    cy.get('#quantity').clear().type('1');
    cy.get('#saveItem').click();

    cy.get('#product').select('Hobby');
    cy.get('#quantity').clear().type('2');
    cy.get('#saveItem').click();
});


Then(/^skal handlekurven inneholde det jeg har lagt inn$/, () => {
    cy.get('#cart').should('contain', '4 Hubba bubba');
    cy.get('#cart').should('contain', '5 Smørbukk');
    cy.get('#cart').should('contain', '1 Stratos');
    cy.get('#cart').should('contain', '2 Hobby');
});

And(/^den skal ha riktig totalpris$/, function () {
        cy.get('#price').should('have.text', '33');
});



//scenario2
And(/^jeg har lagt inn varer og kvanta$/, function () {
    cy.get('#product').select('Hubba bubba');
    cy.get('#quantity').clear().type('4');
    cy.get('#saveItem').click();
});

When(/^jeg sletter varer$/, () => {
    cy.get('#deleteItem').click();
});

Then(/^skal ikke handlekurven inneholde det jeg har slettet$/, () => {
    cy.get('#cart').should('not.contain', 'Hubba bubba');
});

//scenario 3
When(/^jeg oppdaterer kvanta for en vare$/, () => {
    cy.get('#product').select('Hubba bubba');
    cy.get('#quantity').clear().type('2');
    cy.get('#saveItem').click();
});

Then(/^skal handlekurven inneholde riktig kvanta for varen$/, () => {
    cy.get('#cart').should('contain', '2 Hubba bubba');
});

//scenario 4
Given(/^at jeg har lagt inn varer i handlekurven$/, function () {
    cy.visit('http://localhost:8080');
    cy.get('#product').select('Hubba bubba');
    cy.get('#quantity').clear().type('4');
    cy.get('#saveItem').click();
});

And(/^trykket på Gå til betaling$/, () => {
    cy.get('#goToPayment').click();
});

When(/^jeg legger inn navn, adresse, postnummer, poststed og kortnummer$/, () => {
    cy.get('#fullName').clear().type('Lene Hansen');
        cy.get('#address').clear().type('Glassvegen 12');
        cy.get('#postCode').clear().type('7060');
        cy.get('#city').clear().type('Oslo');
        cy.get('#city').clear().type('Oslo');
        cy.get('#creditCardNo').clear().type('0000000000000000');
});

And(/^trykker på Fullfør kjøp$/, () => {
    cy.get('input[type=submit]').click();
});

Then(/^skal jeg få beskjed om at kjøpet er registrert$/, () => {
    cy.get('.confirmation').should('contain', 'registrert');
});


//Scenario 5
When(/^jeg legger inn ugyldige verdier i feltene$/, () => {
    cy.get('#fullName').clear().blur();
        cy.get('#address').clear().blur();
        cy.get('#postCode').clear().blur();
        cy.get('#city').clear().blur();
        cy.get('#city').clear().blur();
        cy.get('#creditCardNo').clear().blur();
});

Then(/^skal jeg få feilmeldinger for disse$/, () => {
    cy.get('#fullNameError').should('contain', 'Feltet må ha en verdi');
    cy.get('#addressError').should('contain', 'Feltet må ha en verdi');
    cy.get('#postCodeError').should('contain', 'Feltet må ha en verdi');
    cy.get('#cityError').should('contain', 'Feltet må ha en verdi');
    cy.get('#creditCardNoError').should('contain', 'Kredittkortnummeret må bestå av 16 siffer');
});
